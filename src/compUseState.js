import { useState } from 'react';
import CompUseStateControl from './compUseStateControl'
import CompUseStateRender from './compUseStateRender'
import CompUseStateRenderStatic from './compUseStateRenderStatic'
import CompUseStateCompute from './compUseStateCompute'

function App() {
  console.log('rendering CompUseState')
  const [counterUseState, setCounterUseState] = useState(0)
  const [staticUseState] = useState(0)

  return (
    <div>
        CompUseState <p/>
        <CompUseStateControl counterUseState={counterUseState} setCounterUseState={setCounterUseState}/>
        <CompUseStateRender counterUseState={counterUseState} />
        <CompUseStateRenderStatic staticUseState={staticUseState} />
        <CompUseStateCompute/>
    </div>
  );
}

export default App;