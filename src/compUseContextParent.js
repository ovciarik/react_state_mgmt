import { CompUseContextContextProvider } from "./compUseContextContext";
import CompUseContext from "./compUseContext";

function App() {

  console.log('rendering CompUseContext')
  return (
    <CompUseContextContextProvider>
      <div>
          CompUseContextParent<p/>
          <CompUseContext/>
      </div>
    </CompUseContextContextProvider>
  );
}

export default App;