import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './reduxCounterSlice'

function App({dispatch}) {
  console.log('rendering CompUseReducerControl')


  return (
    <div>
        CompUseReducerControl<p/>
        <button onClick={() => dispatch({ type: 'INC' })}>+1</button>
    </div>
  );
}

export default App;