import CompReduxControl from './compReduxControl'
import CompReduxRender from './compReduxRender'
import CompReduxRenderStatic from './compReduxRenderStatic'
import CompReduxCompute from './compReduxCompute'

function App() {
  console.log('rendering CompRedux')

  return (
    <div>
        CompRedux <p/>
        <CompReduxControl/>
        <CompReduxRender/>
        <CompReduxRenderStatic/>
        <CompReduxCompute/>
    </div>
  );
}

export default App;