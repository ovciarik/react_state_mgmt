import React, { useContext } from "react";
import { ACTIONS, CompUseContextContext } from "./compUseContextContext";

function App() {
  console.log('rendering CompUseContextControl')
  const { dispatch } = useContext(CompUseContextContext);

  return (
    <div>
        CompUseContextControl<p/>
        <button onClick={() => dispatch({ type: ACTIONS.INC })}>+1</button>
    </div>
  );
}

export default App;