import React from 'react'
import { useSelector } from 'react-redux'

function App() {
  console.log('rendering CompReduxRenderStatic')
  const count = useSelector(state => state.counter.static)



  return (
    <div>
        CompReduxRenderStatic <p/>
        Counter: {count} <p/>
    </div>
  );
}

export default App;