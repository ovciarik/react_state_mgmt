function App({counterUseState}) {
  console.log('rendering CompUseStateRender')
  return (
    <div>
        CompUseStateRender <p/>
        Counter: {counterUseState} <p/>
    </div>
  );
}

export default App;