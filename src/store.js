import { configureStore } from '@reduxjs/toolkit'
import counterReducer from './reduxCounterSlice'


export default configureStore({
  reducer: {
    counter: counterReducer
  }
})