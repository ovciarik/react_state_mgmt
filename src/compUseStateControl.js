function App({counterUseState, setCounterUseState}) {
  console.log('rendering CompUseStateControl')
  return (
    <div>
        CompUseStateControl<p/>
        <button onClick={() => setCounterUseState(counterUseState+1)}>+1</button>
    </div>
  );
}

export default App;