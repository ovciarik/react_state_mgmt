import React, { useContext } from "react";
import { CompUseContextContext } from "./compUseContextContext";

function App() {
  console.log('rendering CompUseContextRenderStatic')
  const { state } = useContext(CompUseContextContext);

  return (
    <div>
        CompUseContextRenderStatic<p/>
        Counter: {state.static} <p/>
    </div>
  );
}

export default App;