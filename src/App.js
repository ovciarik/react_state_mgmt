import CompUseState from './compUseState'
import CompUseContextParent from './compUseContextParent'
import CompUseReducer from './compUseReducer'
import CompRedux from './compRedux'

function App() {
  return (
    <div>
      <CompUseState />
------------------------------------------------------------<p/>
      <CompUseContextParent />
------------------------------------------------------------<p/>
      <CompUseReducer />
------------------------------------------------------------<p/>
      <CompRedux />
    </div>
  );
}

export default App;
