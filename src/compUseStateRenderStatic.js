function App({staticUseState}) {
  console.log('rendering CompUseStateRenderStatic')
  return (
    <div>
        CompUseStateRender <p/>
        Counter: {staticUseState} <p/>
    </div>
  );
}

export default App;