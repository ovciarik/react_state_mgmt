import React from 'react'
import { useSelector } from 'react-redux'

function App() {
  console.log('rendering CompReduxRender')
  const count = useSelector(state => state.counter.value)


  return (
    <div>
        CompReduxRender <p/>
        Counter: {count} <p/>
    </div>
  );
}

export default App;