import React, { useReducer } from 'react'
import CompUseReducerControl from './compUseReducerControl'
import CompUseReducerRender from './compUseReducerRender'
import CompUseReducerRenderStatic from './compUseReducerRenderStatic'
import CompUseReducerCompute from './compUseReducerCompute'

console.log('module CompUseReducer');

const types = {
  INC: 'INC',
}

const reducer = (state, action) => {
  switch (action.type) {
    case types.INC:
      return { ...state, count: state.count+1 }
    default:
      return {...state}
  }
}

const initialState = {
  count: 0,
  static: 0
}

function App() {
  console.log('rendering CompUseReducer')
  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    <div>
        CompUseReducer<p/>
        <CompUseReducerControl dispatch={dispatch}/>
        <CompUseReducerRender state={state}/>
        <CompUseReducerRenderStatic state={state}/>
        <CompUseReducerCompute/>
    </div>
  );
}

export default App;