import React, { useContext } from "react";
import { CompUseContextContext } from "./compUseContextContext";

function App() {
  console.log('rendering CompUseContextRender')
  const { state } = useContext(CompUseContextContext);

  return (
    <div>
        CompUseContextRender<p/>
        Counter: {state.value} <p/>
    </div>
  );
}

export default App;