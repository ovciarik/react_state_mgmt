import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { decrement, increment } from './reduxCounterSlice'

function App() {
  console.log('rendering CompReduxControl')
  const dispatch = useDispatch()


  return (
    <div>
        CompReduxControl<p/>
        <button onClick={() => dispatch(increment())}>+1</button>
    </div>
  );
}

export default App;