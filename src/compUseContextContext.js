import React, { useReducer, createContext, useMemo } from "react";
export const CompUseContextContext = createContext(); 

export const ACTIONS = {
  INC: "INC",
};

const initialState = {
  value: 0,
  static: 0
};

const reducer = (state, action) => {
  switch (action.type) {
    case ACTIONS.INC:
      return {
        ...state,
        value: state.value + 1
      };
    default:
      return state;
  }
};

export const CompUseContextContextProvider = ({ children }) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const contextValue = useMemo(() => {
    return { state, dispatch };
  }, [state, dispatch]);
    return (
    <CompUseContextContext.Provider value={contextValue}>
      {children}
    </CompUseContextContext.Provider>
  );
};