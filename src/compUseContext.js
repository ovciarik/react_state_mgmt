import CompUseContextControl from "./compUseContextControl";
import CompUseContextRender from "./compUseContextRender";
import CompUseContextRenderStatic from "./compUseContextRenderStatic";
import CompUseContextCompute from "./compUseContextCompute";

function App() {

  console.log('rendering CompUseContext')
  return (
      <div>
          CompUseContext<p/>
          <CompUseContextControl/>
          <CompUseContextRender/>
          <CompUseContextRenderStatic/>
          <CompUseContextCompute/>
      </div>
  );
}

export default App;